jQuery(document).ready(function(){
    jQuery('#html-add-field').click(function () {
        var id = jQuery('.html-custom-url').last().data('id');
        var new_id = 1;
        if (typeof id !== 'undefined') {
            new_id = id + 1;
        }

        var template = '\
        <tr>\
            <th scope="row"><label for="html_custom_url[' + new_id + ']">Custom url</label></th>\
            <td>\
            <input class="html-custom-url" data-id="' + new_id + '" type="text" name="html_custom_url[' + new_id + ']">\
            <a href="javascript:;" class="remove-custom-url">Remove</a>\
            </td>\
        </tr>';

        jQuery('.html-custom-urls tbody').append(template);
    });

    jQuery('.html-custom-urls').on('click', '.remove-custom-url', function() {
        jQuery(this).parent().parent().remove();
    });
});