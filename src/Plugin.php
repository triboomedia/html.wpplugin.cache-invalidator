<?php

namespace HTML\CacheInvalidator;

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

use HTML\CacheInvalidator\Admin\SettingsPage;
use HTML\CacheInvalidator\AWS\CloudFrontInvalidator;
use HTML\CacheInvalidator\Stingray\StingrayPurger;
use HTML\CacheInvalidator\Utils\InvalidateUrls;
use HTML\CacheInvalidator\Utils\DBHelper;

final class Plugin
{
    const PLUGIN_EDIT_CAPABILITY = 'html_cache_invalidator_edit';
    const TRANSLATION_DOMAIN = 'html_ci';
    const DB_VERSION_OPTION = 'html_ci_db_version';
    const VERSION = '1.0.0';

    private static $instance;
    private $db;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        $this->db = new DBHelper;

        /**
         * Add plugin actions.
         */
        add_action('admin_notices', [$this, 'showErrorMessage']);
        add_action('admin_enqueue_scripts', [$this, 'addScripts']);
        add_action('admin_init', [$this, 'addPluginCapabilities']);
        add_action('admin_menu', [$this, 'AddMenuPages']);
        add_action('publish_page', [$this, 'invalidateCache'], 99, 1);
        add_action('publish_post', [$this, 'invalidateCache'], 99, 1);
        add_action('deleted_post', [$this, 'invalidateCache'], 99, 1);
    }

    private function __clone() {}

    public function checkConfig(): bool
    {
        return defined('HTML_STINGRAY_LOGIN') && defined('HTML_STINGRAY_PASS') && defined('HTML_STINGRAY_TRACE') &&
            defined('HTML_AWS_DISTRIBUTION_ID') && defined('HTML_AWS_ACCESS_KEY') && defined('HTML_AWS_SECRET_KEY');
    }

    public function showErrorMessage()
    {
        if (! $this->checkConfig()) {
            ?>
            <div class="error">
                <p><?php _e('HTML Cache Invalidator is not configured. Please, fill the fields requested. See README.md for more information.', self::TRANSLATION_DOMAIN); ?></p>
            </div>
            <?php
        }
    }

    public function activate()
    {
        $this->db->createTable();
        add_option(self::DB_VERSION_OPTION, self::VERSION);
    }

    public function deactivate()
    {
        $this->db->dropTable();
        delete_option(self::DB_VERSION_OPTION);
    }

    public function addScripts()
    {
        wp_register_script(
            'html_cache_invalidator',
            plugins_url('assets/js/html_cache_invalidator.js', __DIR__),
            ['jquery'],
            self::VERSION,
            true
        );

        wp_enqueue_script('html_cache_invalidator');
    }

    public function addPluginCapabilities()
    {
        $admin = get_role('administrator');
        $admin->add_cap(self::PLUGIN_EDIT_CAPABILITY);
    }

    public function AddMenuPages()
    {
        $settings_page = new SettingsPage;
        
        add_menu_page(
            __($settings_page::PAGE_TITLE, self::TRANSLATION_DOMAIN),
            __($settings_page::PAGE_TITLE, self::TRANSLATION_DOMAIN),
            self::PLUGIN_EDIT_CAPABILITY,
            $settings_page::PAGE_SLUG,
            [$settings_page, 'render'],
            '',
            $settings_page::MENU_POSITION
        );
    }

    public function invalidateCache(int $post_id)
    {
        if (! $this->checkConfig()) {
            return;
        }

        $urls = new InvalidateUrls($post_id);

        /**
         * Invalidate urls in Stingray servers.
         */
        $stingray = new StingrayPurger($urls->get());
        $stingray->StingrayAllUrls();

        /**
         * Invalidate urls in AWS CloudFront.
         */
        $invalidate = array_merge($urls->get(), $urls->getMedia());
        $cloud_front = new CloudFrontInvalidator($invalidate);
        $cloud_front->invalidateUrls();
    }
}
