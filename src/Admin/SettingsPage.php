<?php

namespace HTML\CacheInvalidator\Admin;

use HTML\CacheInvalidator\Utils\Flash;
use HTML\CacheInvalidator\Utils\DBHelper;

class SettingsPage extends Page
{
    const PAGE_TITLE = 'HTML Cache Invalidator';
    const PAGE_SLUG = 'cache_invalidator';
    const MENU_POSITION = '100';

    protected $db;
    
    public function __construct()
    {
        $this->db = new DBHelper;
    }

    public function render()
    {
        $this->save();
        
        $this->view('index', [
            'message' => Flash::get(),
            'urls' => $this->getUrls(),
        ]);
    }

    protected function save()
    {
        if (! isset($_POST['submit'])) {
            return;
        }

        $this->db->clean();

        foreach ($this->getSanitizedInputs() as $custom_url) {
            $this->db->insert($custom_url);
        }

        Flash::success('Custom urls was saved!');
    }

    protected function getSanitizedInputs(): array
    {
        $inputs = $_POST['html_custom_url'];

        array_walk($inputs, function(&$url) {
            $url = trim(filter_var($url, FILTER_SANITIZE_STRING));
        });

        $inputs = array_filter($inputs, function($url) {
            return $url !== '';
        });

        return $inputs;
    }

    protected function getUrls(): array
    {
        $urls =  $this->db->getAll();

        if (! is_array($urls) || empty($urls))
        {
            $url = new \stdClass;
            $url->url = '';
            $urls[] = $url;
        }

        return $urls;
    }
}
