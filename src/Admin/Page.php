<?php

namespace HTML\CacheInvalidator\Admin;

abstract class Page
{
    protected function view(string $template, array $data)
    {
        extract($data);
        include_once __DIR__ . DIRECTORY_SEPARATOR . "templates/$template.php";
    }

    public abstract function render();
}
