<div class="wrap">
    <h1><?php _e( 'HTML Cache Invalidator', 'html_ci' ) ?></h1>

    <?php if ( $message ): ?>
        <div class="notice notice-<?= $message['status'] ?>"><?= $message['message'] ?></div>
    <?php endif; ?>
    
    <form action="" method="post" enctype="multipart/form-data">
        <table class="form-table html-custom-urls">
            <tbody>
            <?php foreach ($urls as $index => $url): ?>
                <tr>
                    <th scope="row"><label for="html_custom_url[<?= $index ?>]"><?php _e('Custom url', 'html_ci') ?></label></th>
                    <td>
                        <input class="html-custom-url" data-id="<?= $index ?>" type="text" name='html_custom_url[<?= $index ?>]' value="<?= $url->url ?>">
                        <a href="#" class="remove-custom-url">Remove</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <a id="html-add-field" class="button button-primary"><?php _e('Add field', 'html_ci') ?></a>
        <?php submit_button('Save') ?>
    </form>
</div>