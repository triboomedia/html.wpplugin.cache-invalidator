<?php

namespace HTML\CacheInvalidator\AWS;

use Aws\CloudFront\CloudFrontClient;

class CloudFrontInvalidator
{
    const RANDOM_STRING_LENGTH = 16;
    protected $urls = [];

    public function __construct(array $urls)
    {
        $this->urls = $urls;
    }

    public function invalidateUrls()
    {
        $cloudFront = new CloudFrontClient([
            'version'     => 'latest',
            'region'      => 'eu-west-1',
            'credentials' => [
                'key'    => HTML_AWS_ACCESS_KEY,
                'secret' => HTML_AWS_SECRET_KEY
            ]
        ]);

        $result = $cloudFront->createInvalidationAsync([
            'DistributionId' => HTML_AWS_DISTRIBUTION_ID,
            'InvalidationBatch' => [
                'CallerReference' => $this->getCaller(),
                'Paths' => [
                    'Items' => $this->urls,
                    'Quantity' => count($this->urls)
                ]
            ]
        ]);
    }

    protected function getCaller(): string 
    {
        return $this->generateRandomString(self::RANDOM_STRING_LENGTH);
    }
    
    protected function generateRandomString($length = 10): string
    {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
        
        return $randomString;
    }
}
