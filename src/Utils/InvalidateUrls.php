<?php

namespace HTML\CacheInvalidator\Utils;

class InvalidateUrls
{
    protected $urls = [];
    protected $media = [];
    protected $db;
    
    public function __construct(int $post_id)
    {
        $this->db = new DBHelper;

        $urls = [
            str_replace(get_bloginfo('wpurl'), "", get_permalink($post_id)),
        ];
        $this->setPostMedia($post_id);

        $urls = array_merge($urls, $this->getCustomUrls(), $this->getTaxonomiesUrls($post_id));
        $urls = apply_filters('html_add_url_to_invalidate', $urls);

        $this->urls = $urls;
    }

    protected function setPostMedia(int $post_id)
    {
        $media_urls = [];
        $medias = get_attached_media('image', $post_id);

        foreach ($medias as $media) {
            $media_urls[] = parse_url($media->guid, PHP_URL_PATH);
        }

        $this->media = $media_urls;
    }

    public function getMedia(): array
    {
        return $this->media;
    }

    protected function getCustomUrls(): array
    {
        $urls = [];
        $db_urls = $this->db->getUrls();

        foreach ($db_urls as $url) {
            array_push($urls, $url[0]);
        }

        return $urls;
    }

    protected function getTaxonomiesUrls(int $post_id): array
    {
        $urls = [];
        $results = $this->db->getTaxonomy($post_id);

        foreach ($results as $res) {
            $link = get_term_link(intval($res->term_id), $res->taxonomy);
            $urls[] = str_replace(get_bloginfo("siteurl"), "", $link);
        }

        return $urls;
    }

    public function get(): array
    {
        return $this->urls;
    }
}
