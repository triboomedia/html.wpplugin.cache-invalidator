<?php

namespace HTML\CacheInvalidator\Utils;

/**
 * Little class for flash messages.
 */
class Flash 
{
    public static function error( $message ) 
    {
        $_SESSION['cepu_message'] = [
            'message' => $message,
            'status' => 'error',
        ];
    }

    public static function success( $message ) 
    {
        $_SESSION['cepu_message'] = [
            'message' => $message,
            'status' => 'success',
        ];
    }
    
    public static function get() 
    {
        if ( ! isset( $_SESSION['cepu_message'] ) ) {
            return null;
        }
        
        $message = $_SESSION['cepu_message'];
        unset( $_SESSION['cepu_message'] );
        
        return $message;
    }
}
