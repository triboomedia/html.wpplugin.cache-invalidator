<?php

namespace HTML\CacheInvalidator\Utils;

class DBHelper 
{
    const TABLE_NAME = 'html_custom_urls';

    protected $db;
    protected $table;

    public function __construct()
    {
        global $wpdb;

        $this->db = $wpdb;
        $this->table = $wpdb->prefix . self::TABLE_NAME;
    }

    public function createTable()
    {
        $charset_collate = $this->db->get_charset_collate();

        $this->db->query("CREATE TABLE {$this->table} (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            url VARCHAR(150) NOT NULL,
            PRIMARY KEY  (id)
            ) $charset_collate;");
    }

    public function dropTable()
    {
        $this->db->query("DROP TABLE IF EXISTS {$this->table}");
    }

    public function clean()
    {
        $this->db->query("DELETE FROM {$this->table}");
    }

    public function insert($custom_url)
    {
        return $this->db->insert($this->table, [
            'url' => $custom_url,
        ]);
    }
    
    public function getAll()
    {
        return $this->db->get_results("SELECT * FROM {$this->table}");
    }

    public function getUrls()
    {
        return $this->db->get_results("SELECT url FROM {$this->table}", ARRAY_N);
    }

    public function getTaxonomy($post_id)
    {
        $query = "
        SELECT term_id, taxonomy FROM ".$this->db->prefix."term_taxonomy 
        INNER JOIN ".$this->db->prefix."term_relationships 
        ON (".$this->db->prefix."term_taxonomy.term_taxonomy_id = ".$this->db->prefix."term_relationships.term_taxonomy_id) 
        where object_id = " . $post_id;

        return $this->db->get_results($query);
    }
}
