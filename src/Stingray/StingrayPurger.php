<?php

namespace HTML\CacheInvalidator\Stingray;

class StingrayPurger
{
    const STINGRAY_SERVERS = [
            "https://vtm1.aws.tbm.local:9090",
            "https://vtm2.aws.tbm.local:9090",
        ];

    protected $server;

    public function __construct(array $urls)
    {
        $this->server = $_SERVER['SERVER_NAME'];
        $this->urls = $urls;
    }

    public function StingrayAllUrls()
    {
        foreach ($this->urls as $url) {
            $this->StingrayPurgeObject($url);
        }
    }

    protected function StingrayPurgeObject(string $permalink)
    {
        foreach (self::STINGRAY_SERVERS as $url) {
            try {
                $this->purge($url, $permalink);
            } catch (\SoapFault $e) {
                
            } catch (\Exception $e) {
                
            }
        }
    }

    protected function purge(string $url, string $permalink)
    {
        $conn = Stingray::connect($url);
        $conn->__setLocation($url . '/soap');

        if ($permalink === 'www.leonardo.it') {
            $conn->clearMatchingCacheContent('http', 'www.leonardo.it', "/");
        } else {
            $conn->clearMatchingCacheContent('http', $this->getWwwServer(), $permalink);
        }
    }

    protected function getWwwServer(): string
    {
        $explode_server = explode('.', $this->server);

        if (count($explode_server) > 2) {
            $www_server = $this->server;
        } else {
            $www_server = "www." . $this->server;
        }

        return $www_server;
    }
}
