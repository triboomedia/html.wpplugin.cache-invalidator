<?php

namespace HTML\CacheInvalidator\Stingray;

class Stingray
{
    public static function connect(string $server): \SoapClient
    {
        $context = stream_context_create([
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ]);
        
        return new \SoapClient(
            $server . "/apps/zxtm/wsdl/System.Cache.wsdl",
            [
                'login' => HTML_STINGRAY_LOGIN,
                'password' => HTML_STINGRAY_PASS,
                'trace' => HTML_STINGRAY_TRACE,
                'stream_context' => $context
            ]
        );
    }
}
