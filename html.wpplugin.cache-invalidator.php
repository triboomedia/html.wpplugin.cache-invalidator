<?php
/**
 * Plugin name: HTML Cache Invalidator
 * Plugin URI: http://www.html.it
 * Description: Plugin to handle cache invalidation.
 * Version: 1.1.2
 * Author URI: http://www.html.it/
 * License: MIT
 */

require_once __DIR__ . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . 'autoload.php';

use \HTML\CacheInvalidator\Plugin;

$plugin = Plugin::getInstance();

register_activation_hook(__FILE__, [$plugin, 'activate']);
register_deactivation_hook(__FILE__, [$plugin, 'deactivate']);
