# HTML Cache Invalidator

##Configure Stingray and AWS
Set in wp-config.php
```php
define('HTML_STINGRAY_LOGIN', 'login');
define('HTML_STINGRAY_PASS', 'password');
define('HTML_STINGRAY_TRACE', 'trace');
define('HTML_AWS_DISTRIBUTION_ID', 'distribution_id');
define('HTML_AWS_ACCESS_KEY', 'access_key');
define('HTML_AWS_SECRET_KEY', 'secret_key');
```

##Urls manage
You can manage urls with admin panel or with filter function.
```php
add_filter('html_add_url_to_invalidate', 'test_function');

function test_function(array $urls) {
    //change array of urls;

    return $urls;
}
```